
/* *************************************************
 *                                                 *  
 *  File       : Main.java                         *
 *  Author     : Dhivya Selvaraj                   *
 *  Date       : 09-04-2020                        *
 *  Description: This file Main() carries out the  * 
 *  ticket booking&cancellation operations         * 
 *                                                 *
 * *************************************************/

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

public class Main {

	public static void main(String[] args) throws NumberFormatException, IOException {
		// TODO Auto-generated method stub
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));

		TicketBookingDetails tktbookingdetails = new TicketBookingDetails();//Reference to the TicketBookingDetails class

		ArrayList<Integer> bookedList = new ArrayList<Integer>();////List reference to add the booked seats
		ArrayList<Integer> removeBookedlist = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));//List reference to remove the booked seats
		int choice;
		String movie1 = "Petta", movie2 = "Darbar";//List of movies
		System.out.println("1." + movie1 + " 2." + movie2);

		System.out.println("Tickets available for " + movie1 + " are ");
		tktbookingdetails.seatAvailability();//Available tickets initially for Petta movie

		System.out.println("Tickets available for " + movie2 + " are ");
		tktbookingdetails.seatAvailability();//Available tickets initially for Darbar movie

		System.out.println("Enter the movie of your choice");
		String movName = bufferedreader.readLine();//Reading user input as movie name
		
		/* Reading user input and switch to the choice accordingly
		 * For booking tickets, remove the seat number from the removeBookedlist
		 * For canceling tickets, add the seat number to removeBookedlist
		
         */
		do {
			System.out.println("");

			System.out.println("\n1.Book seats \n2.Cancel tickets \n3.Exit");
			System.out.println("Enter your choice");
			choice = Integer.parseInt(bufferedreader.readLine());
			if (choice >= 3) {
				choice = 3;
			}
			switch (choice) {
			case 1:
				System.out.println("Enter the Seat No.");
				int seatNo = Integer.parseInt(bufferedreader.readLine());

				if (seatNo > 9)

				{
					System.out.println("Invalid seat number");
					choice = 3;
					break;
				}

				if (removeBookedlist.contains(seatNo)) {
					removeBookedlist.remove(new Integer(seatNo));
					System.out.println("The following tickets has been booked\n" + seatNo);
				} else {
					System.out.println(
							"You have entered the seat which was booked already. Hence please check for some other seats.");
				}

				bookedList.add(seatNo);

				if (!removeBookedlist.isEmpty()) {
					System.out.println("Seats available for booking");
					Iterator<Integer> it = removeBookedlist.iterator();
					while (it.hasNext()) {
						System.out.print(it.next()+ " ");
					}
				} else {
					System.out.println("No available tickets.Please check later");
					choice = 2;
				}
				break;

			case 2:
				System.out.println("Cancelling Tickets");
				System.out.println("Enter the Seat No. to be Cancelled");
				int seatNoCancel = Integer.parseInt(bufferedreader.readLine());
				
				if (seatNoCancel > 9)

				{
					System.out.println("Invalid seat number");
					choice = 3;
					break;
				}

				System.out.println("Your request is being processed.........");
				if (bookedList.contains(seatNoCancel)) {
					System.out.println("The following tickets has been cancelled\n" + seatNoCancel);
					removeBookedlist.add(seatNoCancel);
					Collections.sort(removeBookedlist);
				} else {
					System.out.println("The seat " + seatNoCancel
							+ " has not yet booked. Hence can not be cancelled. Please check for valid booked seats");
				}
				if (!removeBookedlist.isEmpty()) {
					System.out.println("Seats available for booking after cancellation");
					Iterator<Integer> it = removeBookedlist.iterator();
					while (it.hasNext()) {
						System.out.print(it.next()+" ");
					}
				}
				break;

			}

		}

		while (choice != 3);

	}

}
