/* *************************************************
 *                                                 *  
 *  File       : Main.java                         *
 *  Author     : Dhivya Selvaraj                   *
 *  Date       : 09-04-2020                        *
 *  Description: This file TicketBookingDetails()  * 
 *  prints the initial ticket availability         * 
 *  Function seatAvailability() prints the rows    *
 *  and columns of seat available for first time   *
 * *************************************************/

public class TicketBookingDetails {
	public void seatAvailability() {
		int seatAva[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		for (int row = 0; row <= 2; row++) {
			for (int col = 0; col <= 2; col++) {

				System.out.print(seatAva[row][col] + " ");
			}
			System.out.println();
		}

	}
}

